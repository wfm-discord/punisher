import discord
from discord.utils import get
from discord.ext import commands
import math
import time
import asyncio
import sqlite3
from utils.database import add_count_replace, add_count_ignore, get_count

# import environment variables

import os

# can now get variables from .env os.environ.get("VARIABLE")

bot = commands.Bot(command_prefix=commands.when_mentioned_or("?"))

# Bot Events
@bot.event
async def on_ready():
    print("bot is online")

    con = sqlite3.connect(os.environ.get("BRAIN_DATABASE"))
    cur = con.cursor()
    cur.execute('''CREATE TABLE IF NOT EXISTS counting
                            (user_id text PRIMARY KEY,
                             last_fail integer)''')
    con.commit()

@bot.event
async def on_message(message):
    if 'RUINED IT AT' in message.content and str(message.channel.id) == os.environ.get("COUNTING_CHANNEL_ID"):
        get_number = message.content.split("**")
        failed_number = get_number[1]
        get_user = message.mentions[0].id

        failed_number = int(failed_number)
        failed_number = calculate_punishment(failed_number)
        add_count_replace(get_user,failed_number)
    await bot.process_commands(message)

# Functions
def is_not_pinned(mess):

    return not mess.pinned

def calculate_punishment(failed_number):
    if failed_number < 100:
        return 100
    if 100 <= failed_number <= 300:
        punishment = 100 + math.ceil(failed_number/3)
        return punishment
    else:
        return 250

#Bot Commands
@bot.command()
async def countingDuel(ctx, player2: discord.User):
    if str(ctx.channel.id) != os.environ.get("COUNTING_DUEL_CHANNEL_ID"):
        return
    emoji1 = '\N{WHITE HEAVY CHECK MARK}'
    emoji2 = '\N{Ballot Box with Check}'
    player1 = ctx.author
    await ctx.send("**Counting-Duel** between " + str(player1.mention) + " and " + str(player2.mention) + " is started!\n" + str(player1.mention) + " Please start with the first number:")
    gamestatus = True
    toPlay = player1
    nottoPlay = player2
    count = 1

    message = await bot.wait_for('message', check=lambda message: message.author == player1)
    if message.content.isdigit():
        number = int(message.content)
        if number == count:
            if count % 2 == 0:
                await message.add_reaction(emoji1)
            else:
                await message.add_reaction(emoji2)
            temp = toPlay
            toPlay = nottoPlay
            nottoPlay = temp
            count = count + 1
        else:
            await ctx.send("wrong number " + toPlay.mention)
            await ctx.send("**Game Over!!!** " + nottoPlay.mention + " is the **WINNER!**")
            gamestatus = False
            return
    else:
        await ctx.send("that is not even a number " + toPlay.mention)
        await ctx.send("**Game Over!!!** " + nottoPlay.mention + " is the **WINNER!**")
        gamestatus = False
        return

    while gamestatus == True:
        try:
         message = await bot.wait_for('message',check=lambda message: message.author == toPlay,timeout=2.5)
         if message.content.isdigit():
            number = int(message.content)
            if number == count:
                if count % 2 == 0:
                    await message.add_reaction(emoji1)
                else:
                    await message.add_reaction(emoji2)
                temp = toPlay
                toPlay = nottoPlay
                nottoPlay = temp
                count = count +1
            else:
                await ctx.send("wrong number " + toPlay.mention)
                await ctx.send("**Game Over!!!** " + nottoPlay.mention + " is the **WINNER!**")
                gamestatus = False
                return
         else:
            await ctx.send("that is not even a number " + toPlay.mention)
            await ctx.send("**Game Over!!!** " + nottoPlay.mention + " is the **WINNER!**")
            gamestatus = False
            return
        except asyncio.TimeoutError:
            await ctx.send("too late " + toPlay.mention)
            await ctx.send("**Game Over!!!** " + nottoPlay.mention + " is the **WINNER!**")
            gamestatus = False
            return


@bot.command()
async def punish(ctx):
    if str(ctx.channel.id) != os.environ.get("CORNER_OF_SHAME_ID"):
        await ctx.send("This command can only be used in the Corner of Shame.")
        return
    member = ctx.author
    placeholder_goal = 100
    add_count_ignore(member.id,placeholder_goal)
    count = 1
    goal = get_count(member.id)

    emoji = '\N{WHITE HEAVY CHECK MARK}'

    await ctx.send(
        "You've acted shamefully!\nNow you are in **Corner of Shame**.\nCount up to " + str(goal) + " to get out!")
    while count != goal+1:
        message = await bot.wait_for('message', check=lambda m: m.author == member)
        if (str(message.channel.id) != os.environ.get("CORNER_OF_SHAME_ID") and message.content.isdigit()):
            await message.channel.send("SHAME ON YOU for trying to count outside of the Corner of Shame! I'm increasing your punishment!")
            goal += 100
            await ctx.send("<@" + str(member.id) + ">'s goal is now " + str(goal) + " for trying to count outside of the corner.");
        else:
            if (message.content == "stoppunish"):

                deleted = await ctx.channel.purge(limit=200, check=is_not_pinned)
                return

            if message.content.isdigit():
                number = int(message.content)
                if (number == count):
                    count = count + 1
                    await message.add_reaction(emoji)
                else:
                    await ctx.send(":x:wrong:x: \nYour next number is **" + str(count) + '**')
            else:
                await ctx.send(":x:wrong:x: \nYour next number is **" + str(count) + '**')

    role = get(ctx.author.guild.roles, name="Corner of Shame")
    await ctx.author.remove_roles(role)
    await ctx.send("You are released from **Corner of Shame**")
    add_count_replace(member.id, 100)
    time.sleep(10)
    deleted = await ctx.channel.purge(limit=300, check=is_not_pinned)


#Token
bot.run(os.environ.get("DISCORD_TOKEN"))
