

#Getting the bot running:

This bot uses pipenv.

- `pipenv shell` to make a new shell
- `pipenv install` to install dependencies
- `pipenv run python main_punisher.py` to run the bot
