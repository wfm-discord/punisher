import sqlite3

# import environment variables
import os


def add_count_replace(user_id: int, last_fail: int) -> None:
    """
    Adds a user to the counting database or replaces their last fail count.

    Args:
        user_id (int): The ID of the user to add/replace.
        last_fail (int): The last fail count of the user.

    Returns:
        None
    """
    con = sqlite3.connect(os.environ.get("BRAIN_DATABASE"))
    cur = con.cursor()

    cur.execute('''INSERT OR REPLACE INTO counting VALUES
                            (?,?)''', (user_id, last_fail))

    con.commit()


def add_count_ignore(user_id: int, last_fail: int) -> None:
    """
    Adds a user to the counting database, ignoring if they already exist.

    Args:
        user_id (int): The ID of the user to add.
        last_fail (int): The last fail count of the user.

    Returns:
        None
    """
    con = sqlite3.connect(os.environ.get("BRAIN_DATABASE"))
    cur = con.cursor()

    cur.execute('''INSERT OR IGNORE INTO counting VALUES
                            (?,?)''', (user_id, last_fail))

    con.commit()


def get_count(user_id: int) -> int:
    """
    Returns the last fail count of a user from the counting database.

    Args:
        user_id (int): The ID of the user to get the count for.

    Returns:
        int: The last fail count of the user.
    """
    con = sqlite3.connect(os.environ.get("BRAIN_DATABASE"))
    cur = con.cursor()

    cur.execute("SELECT last_fail FROM counting WHERE user_id=?", [user_id])
    data = cur.fetchone()

    con.commit()

    return data[0]
